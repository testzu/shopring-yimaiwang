package com.jk.mapper;

import com.jk.model.User;

public interface UserMapper {
    void addUser(User user);
}
