package com.jk.app;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;



@SpringBootApplication
@EnableEurekaClient
@ComponentScan(basePackages = "com.jk.*")
@MapperScan("com.jk.mapper")
public class SpringcloudProviterApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringcloudProviterApplication.class, args);
    }

}

