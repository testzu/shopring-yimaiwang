package com.jk.controller;

import com.jk.mapper.UserMapper;
import com.jk.model.User;

import com.jk.service.UserService;
import com.netflix.discovery.converters.Auto;
import jdk.nashorn.internal.ir.annotations.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
public class UserController implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Value("111111")
    String port;
    @GetMapping("/test")
    public String test(){
        System.out.println("请求生产者成功");
        return "success";
    }
    @Override
    public User selectUser(@PathVariable("id") Integer userId){
        User user = new User();
       // user.setUserId(Integer.parseInt(port));
        user.setAddress("北京明园大学");
        user.setUserName("听话");
        userMapper.addUser(user);
        return user;
    }
    @Override
    public Map<String, Object> insertUser(@RequestBody User user){
        System.out.println(user);
        Map<String, Object> map = new HashMap();
        map.put("user", user);
        System.out.println("from port is " + port);
        return map;
    }
}
