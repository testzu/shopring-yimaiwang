package com.jk.service;

import com.jk.model.User;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;
import java.util.Map;
@RequestMapping("error")
@Component
public class UserServiceError implements UserServiceFeign{
    public static final String HYSTRIX_ERROR_FLAG = "网络异常";
    @Override
    public User selectUser(Integer userId) {
        System.out.println("熔断器");
        User user = new User();
        //如果生产者宕机，会进熔断器类，在此类请求的某个通过
        // 状态标识来返回给页面
        // 状态标识可以定义静态常量
        return user;
    }
    @Override
    public Map<String, Object> insertUser(User user) {
        Map<String, Object> map = new HashMap();
        map.put("flag", HYSTRIX_ERROR_FLAG);
        return map;
    }

}

