package com.jk.service;


import org.springframework.cloud.openfeign.FeignClient;

import java.util.Map;
@FeignClient(value = "springcloudproviter",fallback = UserServiceError.class) //指定生产者实例名
public interface UserServiceFeign  extends UserService{

}
