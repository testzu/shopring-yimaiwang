package com.jk.controller;

import com.jk.model.User;
import com.jk.service.UserServiceFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Controller
public class UserController {

    @Resource
    private UserServiceFeign userService;

    @RequestMapping("/hello/{id}")
    public User findUser(@PathVariable("id") Integer userId){
        return userService.selectUser(userId);
    }
    @Autowired
    private MongoTemplate mongoTemplate;
  @RequestMapping("/testmongodb")
    public User testmongodb(){
      User user = new User();
      user.setAddress("北京天安门");
      user.setUserName("毛爷爷");
      user.setUserId(111);
      mongoTemplate.save(user);
      return user;
    }
    @Autowired
    private RedisTemplate redisTemplate;
    @RequestMapping("/testRedis")
    public User testRedis(){
      User user = new User();
      user.setAddress("北京天安门");
      user.setUserName("毛爷爷");
      user.setUserId(111);
        String uuid = UUID.randomUUID().toString();
      redisTemplate.opsForValue().set(uuid,user,5,TimeUnit.MINUTES);
      return user;
    }


    @RequestMapping("/add")
    public Map<String, Object> addUser(){
        User user = new User();
        user.setUserName("邵东旭");
        user.setAddress("名园");
        return userService.insertUser(user);
    }
    @RequestMapping("testPage")
    public String testPage(){
        return "testPage/testPage";
    }
}

