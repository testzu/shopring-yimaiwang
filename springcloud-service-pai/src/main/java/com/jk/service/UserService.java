package com.jk.service;

import com.jk.model.User;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
//@RequestMapping("user")
public interface UserService {
    @RequestMapping("/selectUser/{id}")
    public User selectUser(@PathVariable("id") Integer userId);

    @PostMapping("/insertUser")
    public Map<String, Object> insertUser(@RequestBody User user);
}
